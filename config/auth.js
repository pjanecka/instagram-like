module.exports = {
    ensureAuth: function(req,res,next){
        if(req.isAuthenticated()){
            return next();
        }
        req.flash('errormsg','please login to view dashboard');
        res.redirect('/users/login');
    }
}